<?php

namespace Drupal\maestro_template_builder\Controller;

use Drupal\Core\Entity\Controller\EntityController;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Controller for dynamic route names.
 */
class MaestroTemplateBuilderController extends EntityController {

  /**
   * {@inheritdoc}
   */
  protected function doGetEntity(RouteMatchInterface $route_match, EntityInterface $_entity = NULL) {
    $route_parameters = $route_match->getParameters();

    if ($machine_name = $route_parameters->get('templateMachineName')) {
      $entity = $this->entityTypeManager->getStorage('maestro_template')->load($machine_name);
    }

    return parent::doGetEntity($route_match, $entity);
  }

}
